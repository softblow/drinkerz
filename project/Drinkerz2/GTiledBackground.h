// 타일식으로 이미지를 반복해서 출력
#pragma once
#include "gimage.h"
class CGTiledBackground :
	public CGImage
{
public:
	CGTiledBackground(UINT nIDRes, CSize size = CSize(0,0), CPoint pos = CPoint(0,0));
	CGTiledBackground(UINT nIDRes, COLORREF  TransColor, CSize size = CSize(0,0), CPoint pos = CPoint(0,0));


	virtual bool DrawImage(CDC& dc, int w=0, int h=0); // w, h 쌩깜
	virtual bool DrawImageTrans(CDC& dc, int w=0, int h=0);
	virtual void UpdateImage(){}

protected:
	CSize m_size;
};

