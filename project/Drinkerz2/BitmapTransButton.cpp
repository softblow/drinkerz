// BitmapTransButton.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Drinkerz2.h"
#include "BitmapTransButton.h"


// CBitmapTransButton

IMPLEMENT_DYNAMIC(CBitmapTransButton, CButton)

CBitmapTransButton::CBitmapTransButton()
{

}

CBitmapTransButton::~CBitmapTransButton()
{
}


BEGIN_MESSAGE_MAP(CBitmapTransButton, CButton)
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEHOVER()
	ON_WM_MOUSELEAVE()
END_MESSAGE_MAP()



BOOL CBitmapTransButton::LoadBitmaps(UINT nIDBitmapResource, UINT nFlags, COLORREF crMask)
{
	HBITMAP hBitmap = (HBITMAP)::LoadImage(AfxFindResourceHandle(MAKEINTRESOURCE(nIDBitmapResource),RT_BITMAP), MAKEINTRESOURCE(nIDBitmapResource), IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap == NULL)
	{
		//TRACE("wtf_failed \n");
		return false;
	}
	m_imgList.DeleteImageList();

	CBitmap bitmap;
	bitmap.Attach(hBitmap);
	{
		BITMAP bm;
		bitmap.GetBitmap(&bm);

		m_szImage.cx = bm.bmWidth >> 2; // 4 등분
		m_szImage.cy = bm.bmHeight;
	}

	m_imgList.Create(m_szImage.cx, m_szImage.cy, nFlags, 4, 0);

	if ((nFlags & ILC_MASK) != 0)
		m_imgList.Add (&bitmap, crMask);
	else
		m_imgList.Add(&bitmap, (CBitmap *)NULL);
	// 정리
	bitmap.DeleteObject();
	return true;
}



// CBitmapTransButton 메시지 처리기입니다.




void CBitmapTransButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{

	// TODO:  지정된 항목을 그리는 코드를 추가합니다.
	if (lpDrawItemStruct == NULL)
	{
		return;
	}
	if (m_imgList.GetSafeHandle() == NULL || m_imgList.GetImageCount() != 4);
	{
		TRACE("failed to init");
		return;
	}

	CRect rect;
	rect.CopyRect(&lpDrawItemStruct->rcItem);

	CPoint pt = rect.TopLeft();
	CDC *pDC = CDC::FromHandle(lpDrawItemStruct->hDC);

	UINT state = lpDrawItemStruct->itemState;

	// 순서 : 
	if ((state & ODS_SELECTED) || (state & ODS_CHECKED))
	{
		m_imgList.Draw(pDC, 1, pt, ILD_TRANSPARENT);
	}
	else if ((state & ODS_DISABLED) || (state & ODS_GRAYED))
	{
		m_imgList.Draw(pDC, 3, pt, ILD_TRANSPARENT);
	}
	else if (m_bHover == TRUE)
	{
		m_imgList.Draw(pDC, 2, pt, ILD_TRANSPARENT);
	}
	else
	{
		m_imgList.Draw(pDC, 0, pt, ILD_TRANSPARENT);
	}
}


void CBitmapTransButton::PreSubclassWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (!ModifyStyle(0,WS_CHILD | WS_VISIBLE | BS_OWNERDRAW | BS_PUSHBUTTON))
		TRACE("failed to init\n");
	CButton::PreSubclassWindow();
}


void CBitmapTransButton::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	
	if (m_bOnMouse == FALSE)
	{
		m_bOnMouse = TRUE;
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.dwFlags = TME_LEAVE | TME_HOVER;
		tme.hwndTrack = GetSafeHwnd();
		tme.dwHoverTime = 1; // HOVER_DEFAULT;
		m_bOnMouse = ::_TrackMouseEvent(&tme);
	}
	CButton::OnMouseMove(nFlags, point);
}


void CBitmapTransButton::OnMouseHover(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_bHover = TRUE;
	Invalidate();
	CButton::OnMouseHover(nFlags, point);
}


void CBitmapTransButton::OnMouseLeave()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_bOnMouse = FALSE;
	m_bHover = FALSE;
	Invalidate();
	CButton::OnMouseLeave();
}
