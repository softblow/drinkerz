#include "StdAfx.h"
#include "GImage.h"


CGImage::CGImage(UINT nIDRes, CPoint pos) :
	m_pos(pos),
	m_pivot(0,0),
	m_transColor(RGB(255,0,255))
{
	m_bitmap.LoadBitmap(nIDRes);
}
CGImage::CGImage(UINT nIDRes, COLORREF  TransColor, CPoint pos)
	:
	m_pos(pos),
	m_transColor(TransColor),
	m_pivot(0,0)
{
	m_bitmap.LoadBitmap(nIDRes);
}
CGImage::CGImage(HBITMAP hbitmap, CPoint pos) :
	m_pos(pos),
	m_pivot(0,0),
	m_transColor(RGB(255,0,255))
{
	CopyBitmap(hbitmap, &m_bitmap);
}
CGImage::CGImage(HBITMAP hbitmap, COLORREF  TransColor, CPoint pos)
	:
	m_pos(pos),
	m_transColor(TransColor),
	m_pivot(0,0)
{
	//TRACE("on");
	CopyBitmap(hbitmap, &m_bitmap);
	//TRACE("move");
}


CGImage::~CGImage(void)
{
	
}


bool CGImage::DrawImage(CDC& dc, int w, int h)
{
	if (m_bitmap.GetSafeHandle() == NULL)
		return false;

	BITMAP bm;
	m_bitmap.GetBitmap(&bm);
	
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CBitmap* pOldBitmap = (CBitmap *)dcMem.SelectObject(&m_bitmap);
	
	dc.BitBlt(m_pos.x - m_pivot.x, m_pos.y - m_pivot.y, w, h, &dcMem, 0, 0, SRCCOPY);
	
	if (pOldBitmap != NULL)
		dcMem.SelectObject (pOldBitmap);

	return true;
}


bool CGImage::DrawImageTrans(CDC& dc, int w, int h)
{
	if (m_bitmap.GetSafeHandle() == NULL)
		return false;

	BITMAP bm;
	m_bitmap.GetBitmap(&bm);
	
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CBitmap* pOldBitmap = (CBitmap *)dcMem.SelectObject(&m_bitmap);
	
	dc.TransparentBlt(m_pos.x - m_pivot.x, m_pos.y - m_pivot.y, min(w, bm.bmWidth), min(h, bm.bmHeight), &dcMem, 0, 0, min(w, bm.bmWidth), min(h, bm.bmHeight), m_transColor);
	
	if (pOldBitmap != NULL)
		dcMem.SelectObject (pOldBitmap);

	return true;
}

//http://www.camaswood.com/tech/copy-hbitmap-to-cbitmap/
void CGImage::CopyBitmap(HBITMAP hBmp, CBitmap *oBmp)
{
    CBitmap *srcBmp;
    srcBmp = CBitmap::FromHandle(hBmp);

    BITMAP bmpInfo;
    srcBmp->GetBitmap(&bmpInfo);

    CDC srcDC;
    srcDC.CreateCompatibleDC(NULL);

    CBitmap *pOldBmp1 = srcDC.SelectObject(srcBmp);
    oBmp->CreateCompatibleBitmap(&srcDC,bmpInfo.bmWidth,bmpInfo.bmHeight);

    CDC destDC;
    destDC.CreateCompatibleDC(NULL);

    CBitmap *pOldBmp2 = destDC.SelectObject(oBmp);
    destDC.BitBlt(0,0,bmpInfo.bmWidth,bmpInfo.bmHeight,&srcDC,0,0,SRCCOPY);

    srcDC.SelectObject(pOldBmp1);
    destDC.SelectObject(pOldBmp2);
}