#pragma once

#include "gimage.h"
#include "GPlayer.h"
#include "resource.h"

class CPlayer;

class CGMap :
	public CGImage
{
public:
	enum MAPTYPE {Normal = IDBM_REST, Attraction = IDBM_ATTRACTION, Event = IDBM_EVENT, Game = IDBM_GAME, End = IDBM_END}
	m_maptype;

public:
	CGMap(CPoint pos = CPoint(0,0));
	CGMap(COLORREF TransColor, CPoint pos = CPoint(0,0));
	CGMap(MAPTYPE maptype, CPoint pos = CPoint(0,0));
	CGMap(MAPTYPE maptype, COLORREF  TransColor, CPoint pos = CPoint(0,0) );

	virtual void UpdateImage();

	MAPTYPE RandMapReturn();
	void RandMapChange();

	CGMap* m_next;
	CGMap* m_prev;

};

