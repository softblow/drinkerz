// 께..께임입니다.
#pragma once

#include "GImage.h"
#include "GSprite.h"
#include "GTiledBackground.h"
#include "GPlayer.h"
#include "GMap.h"

#include "RPSGame.h"


class CDrinkerz2Dlg;


class CGameMaster
{
public:
	CGameMaster(CDrinkerz2Dlg& parent);
	~CGameMaster(void);

	void DoState(); // 타이머 없이도 아무때나 부를 수 있습니다. 주로 게임흐름을 처리
	void Update(); // 이미지를 갱신합니다 + 시간에 따라 처리되는 게임흐름 처리 (타이머 쓸걸... 멍청...)
	void Draw(CDC& dc, CDC& dc_parent); // 화면에 그립니다. 볼 게 없음

	CPoint m_viewport;
	// 게임 흐름 관리입니다. 사실상 스파게티입니다. 설명할 마음 없습니다. ㅠㅠ
	// mg_ 로 변수명이 시작합니다. (member, "game management")
	enum STATE{dice, dice_roll, move, check_effect, minigame, show_effect, next_player } mg_state;
	enum SUBSTATE{init, start, middle, end} mg_substate;
	enum EFFECT{gohome, full_recover, map_effect, rest} mg_effect; // used on show effect
	enum MINIGAME {rps, rps_small, nothing} mg_minigame;
	int mg_timer;

protected:
	CDrinkerz2Dlg& mwp_parent; // member window parent

public:
	CGPlayer mp_nahi;
	CGPlayer mp_dagu;
	CGPlayer* mp_current;

protected:
	CGTiledBackground mbk_back; // member BacKground
	//CGMap mmp_test;
	CGMap* mmp_head; // 맵 포인터. mmp : member map pointer
	CGMap* mmp_end;

	int m_dice_num;
	int m_dice_num_remain; // 남은 칸 이동 횟수

	// 구상의 흔적
	//CList <CGImage, CGImage&> ml_map;
	//CArray <CGPlayer, CGPlayer&> ml_players;

	bool mb_enabled; // engine enabled
	CRPSGame* mw_rps; // member Window
	int mw_return;
	bool mb_domodal;

public: // bool_msg_connector
	bool mbi_DiceRoll;

};
