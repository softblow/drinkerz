#pragma once
#include "gimage.h"

class CGSprite :
	public CGImage
{
public:
	CGSprite(UINT nIDRes, CSize frameSize, CPoint pos = CPoint(0,0));
	CGSprite(UINT nIDRes, CSize frameSize, COLORREF  TransColor, CPoint pos = CPoint(0,0));
	~CGSprite();

public:
	typedef struct ANIINFO {
		UINT frame_count;
		UINT frame_max;
		UINT ani_id;
	} ANIINFO;

public:
	void SetAniInfo(ANIINFO aniinfo) {m_aniinfo = aniinfo; m_setAniInfo = true;}

	bool DrawImage(CDC& dc, int w, int h);
	bool DrawImageTrans(CDC& dc, int w, int h);
	
	virtual void UpdateImage();


protected:
	CSize m_offset;
	bool m_setAniInfo;
	ANIINFO m_aniinfo; // 애니메이션 각각의 정보

};