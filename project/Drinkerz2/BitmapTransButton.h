// 사용하지 않은 클래스. 책에 있는 거 그대로 적었는데 동작 안 했습니다.
#pragma once


// CBitmapTransButton

class CBitmapTransButton : public CButton
{
	DECLARE_DYNAMIC(CBitmapTransButton)

public:
	CBitmapTransButton();
	virtual ~CBitmapTransButton();


public:
	BOOL LoadBitmaps(UINT nIDBitmapResource, UINT nFlags = ILC_COLOR24, COLORREF crMask = 0);
	void MoveWindow(int x, int y, BOOL bRepaint = TRUE) { CButton::MoveWindow(x, y, m_szImage.cx, m_szImage.cy, bRepaint);};

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void PreSubclassWindow();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseHover(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();


protected:
	CImageList m_imgList;
	CSize m_szImage;
	BOOL m_bOnMouse;
	BOOL m_bHover;
};


