#include "StdAfx.h"
#include "GSprite.h"


CGSprite::CGSprite(UINT nIDRes, CSize frameSize, CPoint pos) :
	CGImage(nIDRes, pos),
	m_offset(frameSize),
	m_setAniInfo(false)
{
}


CGSprite::CGSprite(UINT nIDRes, CSize frameSize, COLORREF  TransColor, CPoint pos) :
	CGImage(nIDRes, TransColor, pos),
	m_offset(frameSize),
	m_setAniInfo(false)
{
}


CGSprite::~CGSprite(void)
{
	
}



bool CGSprite::DrawImage(CDC& dc, int w, int h)
{
	if (m_bitmap.GetSafeHandle() == NULL)
		return false;

	if (!m_setAniInfo)
		return false;

	BITMAP bm;
	m_bitmap.GetBitmap(&bm);
	
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CBitmap* pOldBitmap = (CBitmap *)dcMem.SelectObject(&m_bitmap);

	dc.BitBlt(m_pos.x - m_pivot.x, m_pos.y - m_pivot.y, w, h, &dcMem, (m_offset.cx * m_aniinfo.frame_count), (m_offset.cy * m_aniinfo.ani_id), SRCCOPY);
	
	if (pOldBitmap != NULL)
		dcMem.SelectObject (pOldBitmap);

	return true;
}


bool CGSprite::DrawImageTrans(CDC& dc, int w, int h)
{
	if (m_bitmap.GetSafeHandle() == NULL)
		return false;

	if (!m_setAniInfo)
		return false;

	BITMAP bm;
	m_bitmap.GetBitmap(&bm);
	
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CBitmap* pOldBitmap = (CBitmap *)dcMem.SelectObject(&m_bitmap);
	
	dc.TransparentBlt(m_pos.x - m_pivot.x, m_pos.y - m_pivot.y, min(w, bm.bmWidth), min(h, bm.bmHeight), &dcMem, (m_offset.cx  *m_aniinfo.frame_count), (m_offset.cy * m_aniinfo.ani_id), min(w, bm.bmWidth), min(h, bm.bmHeight), m_transColor);
	
	if (pOldBitmap != NULL)
		dcMem.SelectObject (pOldBitmap);

	return true;
}

void CGSprite::UpdateImage()
{
	m_aniinfo.frame_count = (m_aniinfo.frame_count + 1) % m_aniinfo.frame_max;

}