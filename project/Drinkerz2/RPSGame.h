// 가위바위보 게임

#pragma once

#include "BitmapTransButton.h"
#include "TransBitStatic.h"

// CRPSGame 대화 상자입니다.

class CRPSGame : public CDialogEx
{
	DECLARE_DYNAMIC(CRPSGame)

public:
	CRPSGame(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRPSGame();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RPSGAME };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnBnClickedRock();
//	afx_msg void OnStnClickedPlayer2();
	CProgressCtrl mprg_RtsTime; // 남은시간 표시
	CTransBitStatic mpp_Enemy; // 적 이미지
	CTransBitStatic mpp_Player; // 플레이어 이미지 (시간상의 문제로 고정)


	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedScissors();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

public:
	enum RPS {rock = 0, scissors = 1, paper = 2};
	RPS m_rpsEnemy;
	RPS m_rpsPlayer;
	CTransBitStatic mrps_paper; // 플레이어 - 빠
	CTransBitStatic mrps_rock; //생략
	CTransBitStatic mrps_scissors;
	CTransBitStatic mrps_enemy_display; // 상대의 가위바위보
//	afx_msg void OnNMCustomdrawRpstime(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC); // 화면 지우기를 재정의해서 수행하지 않고, 특정 상황에만 수행합니다. 깜빡임 방지
	afx_msg void OnStnClickedRock(); // 마법사로 추가해서 다소 혼잡

	CRect m_rect_paper;
	CRect m_rect_rock;
	CRect m_rect_scissors;
	afx_msg void OnStnClickedScissors();
	afx_msg void OnStnClickedPaper();
	afx_msg void OnPaint();

	bool m_redraw_custom;

	int mg_timer;
};
