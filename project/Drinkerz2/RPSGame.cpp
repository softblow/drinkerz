// RPSGame.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Drinkerz2.h"
#include "RPSGame.h"
#include "afxdialogex.h"
#include "Drinkerz2Dlg.h"

// CRPSGame 대화 상자입니다.

IMPLEMENT_DYNAMIC(CRPSGame, CDialogEx)

CRPSGame::CRPSGame(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRPSGame::IDD, pParent)
	,m_redraw_custom(true)
	,mg_timer(0)
{

}

CRPSGame::~CRPSGame()
{
}

void CRPSGame::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RPSTIME, mprg_RtsTime);
	DDX_Control(pDX, IDCPP_ENEMY, mpp_Enemy);
	//  DDX_Control(pDX, IDCPP_PLAYER, mpp_Paper);
	DDX_Control(pDX, IDCPP_PLAYER, mpp_Player);
	DDX_Control(pDX, IDCBRTS_PAPEr, mrps_paper);
	DDX_Control(pDX, IDCBRTS_ROCK, mrps_rock);
	DDX_Control(pDX, IDCBRTS_SCISSORS, mrps_scissors);
	DDX_Control(pDX, IDCBRTSEnemy, mrps_enemy_display);
}


BEGIN_MESSAGE_MAP(CRPSGame, CDialogEx)


	ON_WM_TIMER()
//	ON_NOTIFY(NM_CUSTOMDRAW, IDC_RPSTIME, &CRPSGame::OnNMCustomdrawRpstime)
ON_WM_ERASEBKGND()
ON_STN_CLICKED(IDCBRTS_ROCK, &CRPSGame::OnStnClickedRock)
ON_STN_CLICKED(IDCBRTS_SCISSORS, &CRPSGame::OnStnClickedScissors)
ON_STN_CLICKED(IDCBRTS_PAPEr, &CRPSGame::OnStnClickedPaper)
ON_WM_PAINT()
END_MESSAGE_MAP()




BOOL CRPSGame::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	//srand(time(NULL));

	mprg_RtsTime.SetPos(100);
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	SetTimer(123, 25, 0);

	mrps_rock.GetWindowRect(&m_rect_rock);
	this->ScreenToClient(&m_rect_rock);
	
	mrps_paper.GetWindowRect(&m_rect_paper);
	this->ScreenToClient(&m_rect_paper);

	mrps_scissors.GetWindowRect(&m_rect_scissors);
	this->ScreenToClient(&m_rect_scissors);

	m_rpsPlayer = rock;
	
	// Init RPS Button Images
	//mrps_Paper.SizeToContent();
	m_redraw_custom = true;
	Invalidate();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}




void CRPSGame::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (mprg_RtsTime.GetPos()>0)
	{
		switch(rand()%3)
		{
		case 0:
			mrps_enemy_display.SetBitmap(::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDBRPS_ROCK)));
			m_rpsEnemy = rock;
			break;
		case 1:
			mrps_enemy_display.SetBitmap(::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDBRPS_SCISSORS)));
			m_rpsEnemy = scissors;
			break;
		case 2:
			mrps_enemy_display.SetBitmap(::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDBRPS_PAPER)));
			m_rpsEnemy = paper;
			break;
		}
	}
	else if (  (mprg_RtsTime.GetPos() == 0) && (mg_timer++ == 0))
	{
		mprg_RtsTime.ShowWindow(SW_HIDE);
		//mprg_RtsTime.DestroyWindow();
		TRACE("done");
		m_redraw_custom = true;
		Invalidate();
	}
	else if (  (mprg_RtsTime.GetPos() == 0) && (++mg_timer > 70))
	{
		TRACE("\nend of RPSgame\n");
		switch ((m_rpsEnemy-m_rpsPlayer+6)%3)
		{
		case 1:
			{
				//dc.DrawText(CString("승리"), clirect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
					EndDialog(IDOK); // 승리 : IDOK

				TRACE("win\n");
			}
			break;
		case 2:
			{
				//dc.DrawText(CString("패배"), clirect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
					EndDialog(IDCANCEL); // 승리 : IDOK

				TRACE("loose\n");
			}
			break;
		case 0:
			{
				//dc.DrawText(CString("무승부"), clirect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
				EndDialog(IDABORT); // 무승부 : IDABORT
				TRACE("draw\n");
			}
			break;
		}
	}


	mprg_RtsTime.SetPos(max(mprg_RtsTime.GetPos()-1,0));
	
	Invalidate();
	CDialogEx::OnTimer(nIDEvent);
}


//void CRPSGame::OnNMCustomdrawRpstime(NMHDR *pNMHDR, LRESULT *pResult)
//{
//	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	*pResult = 0;
//}


BOOL CRPSGame::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (m_redraw_custom)
	{
		CDialogEx::OnEraseBkgnd(pDC);
		m_redraw_custom = false;
	}
	return true;
	
}


void CRPSGame::OnStnClickedRock()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (mprg_RtsTime.GetPos() > 0)
		m_rpsPlayer = rock;

	m_redraw_custom = true;
	Invalidate();
}


void CRPSGame::OnStnClickedScissors()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (mprg_RtsTime.GetPos() > 0)
		m_rpsPlayer = scissors;

	m_redraw_custom = true;
	Invalidate();
}


void CRPSGame::OnStnClickedPaper()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (mprg_RtsTime.GetPos() > 0)
		m_rpsPlayer = paper;

	m_redraw_custom = true;
	Invalidate();
}


void CRPSGame::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	{
		CRect rect = m_rect_rock;
		if (m_rpsPlayer == rock)
		{
			rect.top -= 50;
			rect.bottom -= 50;
		}
		mrps_rock.MoveWindow(rect);
	}

	{
		CRect rect = m_rect_scissors;
		if (m_rpsPlayer == scissors)
		{
			rect.top -= 50;
			rect.bottom -= 50;
		}
		mrps_scissors.MoveWindow(rect);
	}
	{
		CRect rect = m_rect_paper;
		if (m_rpsPlayer == paper)
		{
			rect.top -= 50;
			rect.bottom -= 50;
		}
		mrps_paper.MoveWindow(rect);
	}





	if(mprg_RtsTime.GetPos() == 0)
	{

	CRect clirect;
	GetClientRect(&clirect);
		CFont font;
		font.CreatePointFont(600, CString("굴림"), &dc);
		HGDIOBJ p_before = dc.SelectObject(font);

		dc.SetBkMode(TRANSPARENT);
		switch ((m_rpsEnemy-m_rpsPlayer+6)%3)
		{
		case 1:
			{
				dc.DrawText(CString("승리"), clirect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			}
			break;
		case 2:
			{
				dc.DrawText(CString("패배"), clirect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			}
			break;
		case 0:
			{
				dc.DrawText(CString("무승부"), clirect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			}
			break;
		}
		dc.SelectObject(p_before);
	}
	// 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.
}
