#pragma once
#include "stdafx.h"
#include "DrawDC.h"

CDrawDC::CDrawDC(CDC& dc, CWnd* pWnd)
	:	CBackDC (dc, pWnd)
{
}

CDrawDC::~CDrawDC(void)
{
}

void CDrawDC::DrawBitmap(int x, int y, CBitmap &bitmap)
{
	if (bitmap.GetSafeHandle() == NULL)
		return;
		
	BITMAP bm;
	bitmap.GetBitmap(&bm);
	
	CDC dcMem;
	dcMem.CreateCompatibleDC(this);
	CBitmap* pOldBitmap = (CBitmap *)dcMem.SelectObject(&bitmap);
	
	this->BitBlt(x, y, bm.bmWidth, bm.bmHeight, &dcMem, 0, 0, SRCCOPY);
	
	if (pOldBitmap != NULL)
		dcMem.SelectObject (pOldBitmap);
}

void CDrawDC::DrawStretchBitmap(int x, int y, int cx, int cy, CBitmap& bitmap)
{
	if (bitmap.GetSafeHandle() == NULL)
		return;
		
	BITMAP bm;
	bitmap.GetBitmap(&bm);
	
	CDC dcMem;
	dcMem.CreateCompatibleDC(this);
	CBitmap* pOldBitmap = (CBitmap *)dcMem.SelectObject(&bitmap);
	
	this->StretchBlt(x, y, bm.bmWidth, bm.bmHeight, &dcMem, 0, 0, cx, cy, SRCCOPY);
	
	if (pOldBitmap != NULL)
		dcMem.SelectObject (pOldBitmap);
}

void CDrawDC::DrawLimitedImage(int x, int y, int w, int h, CBitmap& bitmap)
{
	if (bitmap.GetSafeHandle() == NULL)
		return;
		
	BITMAP bm;
	bitmap.GetBitmap(&bm);
	
	CDC dcMem;
	dcMem.CreateCompatibleDC(this);
	CBitmap* pOldBitmap = (CBitmap *)dcMem.SelectObject(&bitmap);
	
	this->BitBlt(x, y, w, h, &dcMem, 0, 0, SRCCOPY);
	
	if (pOldBitmap != NULL)
		dcMem.SelectObject (pOldBitmap);
}