#pragma once

#include "stdafx.h"
#include "resource.h"
#include "GameMaster.h"

#include "GImage.h"
#include "Drinkerz2Dlg.h"
#include "RPSGame.h"
#include "IntroDlg.h"

// 헤더를 보는 게 편합니다.

CGameMaster::CGameMaster(CDrinkerz2Dlg& parent) :
	mwp_parent(parent)
	, mb_enabled(true)
	, mp_nahi(IDBP_NAHI,CSize(76,128),RGB(255,0,255),CPoint(0,0),2000,1000,1000)
	, mp_dagu(IDBP_DAGU,CSize(76,128),RGB(255,0,255),CPoint(0,0),1000,2000,1000)
	, mbk_back(IDB_BACKGROUND, CSize(1300,2800))
	//, mmp_test(IDBM_REST, RGB(255,0,255), CPoint(300,300))
	, mg_state(dice), mg_substate(init)
	, mg_timer(0)
	, m_dice_num(1) , m_dice_num_remain(0)
	, mp_current(&mp_nahi)
	, mg_effect(gohome)
	, mg_minigame(rps)
	, mw_return(0)
	, mb_domodal(false)

{
	srand(time(NULL));

	//GameInit
		//Map
	{
		const int top_offset = 250;
		const int x_offset = 180;
		const int y_offset = 50;
		mmp_head = new CGMap(CGMap::MAPTYPE::End, CPoint(150,top_offset-80));
		mmp_head->SetPivot(CPoint(80,40));
		CGMap* mmp_current = mmp_head;
		for (int i=0; i<3; i++)
		{
			for (int j=0; j<7; j++)
			{
				mmp_current->m_next = new CGMap(RGB(255,0,255)
					, CPoint(100+j*x_offset
							,y_offset*12*(i) + j*y_offset + top_offset));
				CGMap* before = mmp_current;
				mmp_current = mmp_current->m_next;
				mmp_current->m_prev = before;
				mmp_current->SetPivot(CPoint(80,40));
			}
			for (int j=0; j<5; j++)
			{
				mmp_current->m_next = new CGMap(RGB(255,0,255)
					, CPoint(100+(5-j)*x_offset
							,y_offset*12*(i) + j*y_offset + y_offset*7 + top_offset));
				CGMap* before = mmp_current;
				mmp_current = mmp_current->m_next;
				mmp_current->m_prev = before;
				mmp_current->SetPivot(CPoint(80,40));
			}
		}
		mmp_end = new CGMap(CGMap::MAPTYPE::End, CPoint(430,top_offset + y_offset*37));
		mmp_end->SetPivot(CPoint(80,40));
		mmp_end->m_prev = mmp_current;
		mmp_current->m_next = mmp_end;
	}
		//Character
		CGSprite::ANIINFO aniinfo_nahi;
		{
			aniinfo_nahi.ani_id = 0;
			aniinfo_nahi.frame_count = 0;
			aniinfo_nahi.frame_max = 10;
		}
		mp_nahi.SetAniInfo(aniinfo_nahi);
		mp_nahi.SetPivot(CPoint(72*1/4,128));
		mp_nahi.m_pos.y = 128;
		mp_nahi.m_pos.x = 72;
		mp_nahi.SetMapPos(mmp_head);

		mp_dagu.SetAniInfo(aniinfo_nahi);
		mp_dagu.SetPivot(CPoint(72*2/4,128));
		mp_dagu.m_pos.y = 128;
		mp_dagu.m_pos.x = 72*2;
		mp_dagu.SetMapPos(mmp_head);
		m_viewport = CPoint(0,0);

		//mmp_test.SetPivot(CPoint(80,40));


}


CGameMaster::~CGameMaster(void)
{

}


void CGameMaster::DoState() // can call ANYTIME
{
	//Modal Safe
	if (&mwp_parent != mwp_parent.GetLastActivePopup())
		return;


	switch(mg_state)
	{
	case dice:
		{
			switch(mg_substate){
			case init:
				{
					if (mp_current->GetDrink() <= 0)
					{
						mg_state = show_effect;
						mg_substate = init;
						mg_effect = rest;
						mp_current->SetDrink(mp_current->GetDrink()+500);
						return;
					}
					else if (mp_current->GetHp() <= 0)
					{
						mg_state = show_effect;
						mg_substate = init;
						mg_effect = rest;
						mp_current->SetHp(mp_current->GetHp()+500);
						return;
					}
					
					TRACE("start of routine\n");
					if (mp_current != &mp_nahi)
					{
						mg_substate = start;
						mbi_DiceRoll = true;
						return;
					}
					mbi_DiceRoll = false;
					mwp_parent.mdi_DiceRoll.EnableWindow(true);
					mwp_parent.mdi_DiceRoll.ShowWindow(SW_SHOW);
					mwp_parent.mdi_DiceRoll.SetFocus();
					
					mg_substate = start;
				}
				break;
			case start:
				{
					if (mbi_DiceRoll == true)
					{
						mbi_DiceRoll = false;
						mp_current->SetDrink(mp_current->GetDrink()+mp_current->m_pDrinkMax/20);
						mp_current->SetHp(mp_current->GetHp()-50);
						mg_state = dice_roll;
						mg_substate = init;
						TRACE("\ndice -> dice_roll\n");
					}
				}
				break;

			}
		}
	case dice_roll:
			switch(mg_substate){
			case init:
				{
					m_dice_num = rand()%6+1;
					mg_substate = start;
					mg_timer = 30;
					mwp_parent.mg_redraw = true;
				}
				break;
			}
	case check_effect:
		{
			switch(mg_substate)
			{
			case init:
				if (mp_nahi.GetMapPos() == mp_dagu.GetMapPos())
				{
					mg_state = minigame;
					mg_substate = init;
					TRACE("same_place");
					break;
				}
				mg_effect = map_effect;
				switch(mp_current->GetMapPos()->m_maptype)
				{
				case CGMap::MAPTYPE::Event:
					mg_state = minigame;
					mg_substate = init;
					mg_minigame = rps_small;
					break;
				case CGMap::MAPTYPE::Normal:
					{
					mg_state = show_effect;
					mg_substate = init;
					mg_effect = map_effect;
					mp_current->SetHp(mp_current->GetHp() + 100);
					mp_current->SetDrink(mp_current->GetDrink() + 100);
					//TRACE("loool %d\n", mp_current->GetHp());
					}
					break;
				case CGMap::MAPTYPE::End:
					{
						int mtemp_return;
						if (mb_domodal == false)
						{
							mb_domodal = true;
							if (&mp_nahi == mp_current)
								mtemp_return = mwp_parent.MessageBox(CString("승리!\n한번 더 할래요?"),CString("게임 종료"),MB_YESNO);
							else
								mtemp_return = mwp_parent.MessageBox(CString("패배!\n한번 더 할래요?"),CString("게임 종료"),MB_YESNO);
							mwp_parent.EndDialog(0);
						
							if (mtemp_return == IDYES)
							{
								CDrinkerz2Dlg dlg;
								dlg.DoModal();
							}
							else if (mtemp_return == IDNO)
							{
								CIntroDlg dlg;
								dlg.DoModal();
							}
							mb_domodal = false;
						}
					}
					break;
				case CGMap::MAPTYPE::Attraction:
					mp_current->SetHp(mp_current->GetHp() - 300);
					mg_state = show_effect;
					mg_substate = init;
					mg_effect = map_effect;
					break;
				case CGMap::MAPTYPE::Game:
					mp_current->SetDrink(mp_current->GetDrink() - 600);
					mg_state = show_effect;
					mg_substate = init;
					mg_effect = map_effect;
					break;
				default:
					mg_state = show_effect;
					mg_substate = init;
					mg_effect = map_effect;
					break;
				}
				break;
			case start:
				break;
			}
		}
		break;
	case minigame:
		switch(mg_substate)
		{
		case init:
			{
				switch(mg_minigame)
				{
				case rps:
				case rps_small:
					if (mb_domodal == false)
					{
						mb_domodal = true;
						mb_enabled = false;
						TRACE("domodal1()");
						mw_rps = new CRPSGame;
						mw_return = mw_rps->DoModal();
						delete mw_rps;
						TRACE("domodal2()");
						mb_enabled = true;
						mg_state = minigame;
						mg_substate = start;
						mb_domodal = false;
					}
					break;
				}
				break;
			}
			break;
		case start:
			{
				switch(mg_minigame)
				{
				case rps:
				case rps_small:
					switch (mw_return)
					{
					case IDOK: // 승리
						//mg_state = next_player;
						//mg_substate = init;
						mg_state = show_effect;
						mg_substate = init;
						if (&mp_nahi == mp_current)
							mg_effect = full_recover;
						else
							mg_effect = gohome;
						TRACE("\nminigame -start win\n");
						break;
					case IDCANCEL: // 패배
						//mg_state = next_player;
						//mg_substate = init;
						mg_state = show_effect;
						mg_substate = init;
						TRACE("\nminigame -start lose\n");
						if (&mp_nahi == mp_current)
							mg_effect = gohome;
						else
							mg_effect = full_recover;
						break;
					case IDABORT: // 비김
						mg_state = next_player;
						mg_substate = init;
						TRACE("minigame -start draw\n");
						mg_effect = map_effect;
						break;
					default:
						break;
					}
					break;
				default:
					break;
				}
			}
			break;
		case end:
			{
			}
			break;
		}
		break;
	
	case next_player:
		{
			switch(mg_substate)
			{
			case init:

				TRACE("\nnext_player : init - %d : mp_current\n", (mp_current == &mp_nahi));
				mg_state = next_player;
				mg_substate = end;
				break;
			case end:
				mp_current = (mp_current == &mp_nahi)?&mp_dagu : &mp_nahi;
				mg_state = dice;
				mg_substate = init;
				break;
			}
		}
		break;

	}



}














void CGameMaster::Update() // TIME BASED CALL
{

	//Modal Safe
	if (&mwp_parent != mwp_parent.GetLastActivePopup())
		return;



	if (!mb_enabled)
		return;
	mp_nahi.UpdateImage();
	mp_dagu.UpdateImage();

	//mp_nahi.m_pos.x++;

	m_viewport = mp_current->m_pos;
	//TRACE("%d,%d\n" , m_viewport.x, m_viewport.y);



	switch(mg_state)
	{
	case dice_roll:
		{
			switch(mg_substate){
			case start:
				{
					if (--mg_timer <= 0)
					{
						mg_state = move;
						mg_substate = init;
						mg_timer = 0;
						//m_dice_num = 3;
						m_dice_num_remain = m_dice_num;
						mwp_parent.mg_redraw = true;
					}
				}
			}
		}
		break;
	case move:
		{
			switch(mg_substate){
			case init:
				{	if (m_dice_num_remain >0)
					{
						if (   ( abs(mp_current->m_pos.x - mp_current->GetMapPos()->m_pos.x) < 20) && ( abs(mp_current->m_pos.y - mp_current->GetMapPos()->m_pos.y) < 20) )
						{
							mp_current->GoNext();
							m_dice_num_remain--;
						}
					}
					else if (  ( abs(mp_current->m_pos.x - mp_current->GetMapPos()->m_pos.x) < 20) && ( abs(mp_current->m_pos.y - mp_current->GetMapPos()->m_pos.y) < 20)  )
					{
						
						mg_state = check_effect;
						mg_substate = init;
						mwp_parent.mg_redraw = true;
					}
				}
			}
		}
		break;
	case check_effect:
		{
			switch (mg_substate)
			{
			case start:
				{
				}
				break;
			}
		}
		break;
		
	case show_effect:
		{
			TRACE("show_effect\n");
			switch (mg_substate)
			{
			case init:
				{
					CGPlayer* mp_else = (&mp_nahi == mp_current)? &mp_dagu : &mp_nahi;
					TRACE("show_effect / init_start\n");
					if (mg_minigame == rps)
					{
						if (mg_effect == gohome)
						{
							mp_current->SetMapPos(mmp_head);
							mp_current->SetDrink(mp_current->GetDrink()-300);
							mp_current->SetHp(mp_current->GetHp()-50);
							mp_current->SetLuck(mp_current->GetLuck()-100);
							mp_else->SetDrink(mp_current->m_pDrinkMax);
							mp_else->SetHp(mp_current->m_pHpMax);
							mp_else->SetLuck(mp_current->m_pLuckMax);
						}
						else if (mg_effect == full_recover)
						{
							mp_else->SetMapPos(mmp_head);
							mp_else->SetDrink(mp_else->GetDrink()-300);
							mp_else->SetHp(mp_else->GetHp()-50);
							mp_else->SetLuck(mp_else->GetLuck()-100);
							mp_current->SetDrink(mp_current->m_pDrinkMax);
							mp_current->SetHp(mp_current->m_pHpMax);
							mp_current->SetLuck(mp_current->m_pLuckMax);
						}
					} else if (mg_minigame == rps_small)
					{
						if (mg_effect == gohome)
						{
							for (int i = 0; i < 3; i++)
							{
								if (mp_current->GetMapPos()->m_prev != NULL)
									mp_current->SetMapPos(mp_current->GetMapPos()->m_prev);
								if (mp_else->GetMapPos()->m_next != NULL)
									mp_else->SetMapPos(mp_else->GetMapPos()->m_next);
							}

							mp_current->SetDrink(mp_current->GetDrink()-300);
							mp_current->SetHp(mp_current->GetHp()-50);
							mp_current->SetLuck(mp_current->GetLuck()-100);
							mp_else->SetDrink(mp_current->GetDrink()+300);
							mp_else->SetHp(mp_current->GetHp()+200);
							mp_else->SetLuck(mp_current->GetLuck()+100);
						}
						else if (mg_effect == full_recover)
						{
							for (int i = 0; i < 3; i++)
							{
								if (mp_current->GetMapPos()->m_next != NULL)
									mp_current->SetMapPos(mp_current->GetMapPos()->m_next);
								if (mp_else->GetMapPos()->m_prev != NULL)
									mp_else->SetMapPos(mp_else->GetMapPos()->m_prev);
							}
							mp_else->SetDrink(mp_else->GetDrink()-300);
							mp_else->SetHp(mp_else->GetHp()-50);
							mp_else->SetLuck(mp_else->GetLuck()-100);
							mp_current->SetDrink(mp_current->GetDrink()+300);
							mp_current->SetHp(mp_current->GetHp()+200);
							mp_current->SetLuck(mp_current->GetLuck()+100);
						}
					}
					mg_timer = 30;
					mg_substate = middle;
					TRACE("show_effect / init_end\n");

				}
				break;

			case middle:
				{
					TRACE("show_effect - middle \n");
					switch (mg_effect)
					{

					case gohome:
						if (--mg_timer <= 0)
						{
							TRACE("showeff_mid_gohome_mg_timer %d\n", mg_timer);
							mg_state = next_player;
							mg_substate = init;
							mwp_parent.mg_redraw = true;
						}
						break;
					case full_recover:
						if (--mg_timer <= 0)
						{
							TRACE("showeff_mid_full-recover_mg_timer %d\n", mg_timer);
							mg_state = next_player;
							mg_substate = init;
							mwp_parent.mg_redraw = true;
						}
						break;

					case map_effect:
					case rest:
						{
								switch (mp_current->GetMapPos()->m_maptype)
								{
								case CGMap::MAPTYPE::Normal:
								
									{
										if (--mg_timer <= 0)
										{
											mg_state = next_player;
											mg_substate = init;
											mwp_parent.mg_redraw = true;
										}
									}
									break;
								default:
									{
										if (--mg_timer <= 0)
										{
											mg_state = next_player;
											mg_substate = init;
											mwp_parent.mg_redraw = true;
										}
									}
									break;
								} // end of mp_current type check switch
						}
						break;
					}
				}
			} // end of mg_substate
		}
		break;
	} // end of switch_mp_state


}








void CGameMaster::Draw(CDC& dc, CDC& dc_parent) // DRAW
{

	//Modal Safe
	if (&mwp_parent != mwp_parent.GetLastActivePopup())
		return;


	if (!mb_enabled)
		return;
	mbk_back.DrawImage(dc,0,0);
	//mmp_test.DrawImageTrans(dc, 300,300);

	//draw_map!!!!!
	dc.SelectStockObject(NULL_BRUSH);
	CPen map_line_pen(PS_SOLID, 20, RGB(100,200,200));
	dc.SelectObject(&map_line_pen);
	dc.MoveTo(mmp_head->m_pos);
	for (CGMap* mmp_current = mmp_head; mmp_current != NULL; mmp_current = mmp_current->m_next)
	{
		dc.LineTo(mmp_current->m_pos);
		dc.MoveTo(mmp_current->m_pos);

	}
	for (CGMap* mmp_current = mmp_head; mmp_current != NULL; mmp_current = mmp_current->m_next)
	{
		mmp_current->DrawImageTrans(dc,160,80);
	}

	//draw_character
	mp_nahi.DrawImageTrans(dc, 76,128);
	mp_dagu.DrawImageTrans(dc, 76,128);

	switch(mg_state)
	{
	case dice_roll:
		{
			switch(mg_substate){
			case start:
				m_dice_num = rand()%6+1;
				CGImage(IDBD_DICEROLL, CPoint(800+20,200)).DrawImage(dc_parent, 200,200);
			case middle:
				{
					
					switch(m_dice_num)
					{
					case 1:
						CGImage(IDBD_DICE1, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
						break;
					case 2:
						CGImage(IDBD_DICE2, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
						break;
					case 3:
						CGImage(IDBD_DICE3, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
						break;
					case 4:
						CGImage(IDBD_DICE4, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
						break;
					case 5:
						CGImage(IDBD_DICE5, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
						break;
					case 6:
						CGImage(IDBD_DICE6, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
						break;
					}
				}
				break;
			}
		}
		break;
	case move:
		{
			switch(m_dice_num)
			{
			case 1:
				CGImage(IDBD_DICE1, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
				break;
			case 2:
				CGImage(IDBD_DICE2, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
				break;
			case 3:
				CGImage(IDBD_DICE3, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
				break;
			case 4:
				CGImage(IDBD_DICE4, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
				break;
			case 5:
				CGImage(IDBD_DICE5, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
				break;
			case 6:
				CGImage(IDBD_DICE6, CPoint(800+20,400)).DrawImage(dc_parent, 200,200);
				break;
			}
		}
		break;

	case show_effect:
		{
			switch (mg_substate)
			{
			case middle:
				{
					switch(mg_effect)
					{
					case map_effect:
						switch (mp_current->GetMapPos()->m_maptype)
						{
						case CGMap::MAPTYPE::Normal:
							{
								CFont font;
								font.CreatePointFont(200, CString("굴림"), &dc_parent);
								HGDIOBJ p_before = dc_parent.SelectObject(font);
								dc_parent.DrawText(CString("휴식합니다.\n체력 + 100\n주량 + 100"), CRect(820,300,1020,500), DT_CENTER );
								dc_parent.SelectObject(p_before);
							}
							break;
						case CGMap::MAPTYPE::Attraction:
							{					
								CFont font;
								font.CreatePointFont(200, CString("굴림"), &dc_parent);
								HGDIOBJ p_before = dc_parent.SelectObject(font);
								dc_parent.DrawText(CString("장애물을\n통과합니다.\n체력 - 300"), CRect(820,300,1020,500), DT_CENTER );
								dc_parent.SelectObject(p_before);
							}
							break;
						case CGMap::MAPTYPE::Game:
							{
								CFont font;
								font.CreatePointFont(200, CString("굴림"), &dc_parent);
								HGDIOBJ p_before = dc_parent.SelectObject(font);
								dc_parent.DrawText(CString("실컷\n마십니다.\n주량 - 600"), CRect(820,300,1020,500), DT_CENTER );
								dc_parent.SelectObject(p_before);
							}
							break;

						case CGMap::MAPTYPE::End:
						case CGMap::MAPTYPE::Event:

						default:
							{
								CFont font;
								font.CreatePointFont(200, CString("굴림"), &dc_parent);
								HGDIOBJ p_before = dc_parent.SelectObject(font);
								dc_parent.DrawText(CString("별 일은\n없었습니다"), CRect(820,300,1020,500), DT_CENTER );
								dc_parent.SelectObject(p_before);
							}
							break;
						} // end of mp_current type check switch
						break;
						

					case gohome:
						{
								CFont font;
								font.CreatePointFont(200, CString("굴림"), &dc_parent);
								HGDIOBJ p_before = dc_parent.SelectObject(font);
							if (mg_minigame == rps)
							{
								
								dc_parent.DrawText(CString("패배자는\n처음으로"), CRect(820,300,1020,500), DT_CENTER );

							}
							else if (mg_minigame == rps_small)
							{
								dc_parent.DrawText(CString("졌으니까\n조금 뒤로\n갈게요!"), CRect(820,300,1020,500), DT_CENTER );
							}
							dc_parent.SelectObject(p_before);
						}
						break;
					case full_recover:
						{
								CFont font;
								font.CreatePointFont(200, CString("굴림"), &dc_parent);
								HGDIOBJ p_before = dc_parent.SelectObject(font);
								if (mg_minigame == rps)
									dc_parent.DrawText(CString("완전히\n회복되었다"), CRect(820,300,1020,500), DT_CENTER );
								else if (mg_minigame == rps_small)
									dc_parent.DrawText(CString("가위바위보\n승리!"), CRect(820,300,1020,500), DT_CENTER );
								dc_parent.SelectObject(p_before);
						}
						break;
					case rest:
						{
								CFont font;
								font.CreatePointFont(200, CString("굴림"), &dc_parent);
								HGDIOBJ p_before = dc_parent.SelectObject(font);
								dc_parent.DrawText(CString("힘들어서\n못\n가겠어요"), CRect(820,300,1020,500), DT_CENTER );
								dc_parent.SelectObject(p_before);
						}
						break;
					}
				}
			} // end of mg_substate
		}
		break;
	} // mg_state end
}