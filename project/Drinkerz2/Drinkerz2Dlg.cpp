// Drinkerz2Dlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Drinkerz2.h"
#include "Drinkerz2Dlg.h"
#include "afxdialogex.h"

#include "BackDC.h"
// CDrinkerz2Dlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDrinkerz2Dlg, CDialogEx)

CDrinkerz2Dlg::CDrinkerz2Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDrinkerz2Dlg::IDD, pParent)
	, m_gameMaster(*this)
{
	
} 

CDrinkerz2Dlg::~CDrinkerz2Dlg()
{
}

void CDrinkerz2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//  DDX_Control(pDX, IDCG_DICEROLL, mgi_DiceRoll);
	DDX_Control(pDX, IDCG_DICEROLL, mdi_DiceRoll);
}


BEGIN_MESSAGE_MAP(CDrinkerz2Dlg, CDialogEx)
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDCG_DICEROLL, &CDrinkerz2Dlg::OnBnClickedDiceroll)
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


// CDrinkerz2Dlg 메시지 처리기입니다.


void CDrinkerz2Dlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_gameMaster.DoState();
	m_gameMaster.Update();
	Invalidate();
	CDialogEx::OnTimer(nIDEvent);
}

#pragma optimize( "g", off ) // Optimization 해제를 안 하면 Release에서 화면이 안 그려져요...
void CDrinkerz2Dlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	CBackDC dc_back(dc, CRect(10,10,1300,2800), m_gameMaster.m_viewport);

	// GameMaster가 관리하고 있는 이미지를 출력합니다. 모든 이미지, 맵, 캐릭터.
	m_gameMaster.Draw(dc_back, dc);

	// 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.
}
#pragma optimize( "g", on )

BOOL CDrinkerz2Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	SetTimer(123,25,NULL);
	PlaySound(MAKEINTRESOURCE(IDRM_GAME),  GetModuleHandle(NULL), SND_ASYNC | SND_RESOURCE | SND_LOOP);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CDrinkerz2Dlg::OnBnClickedDiceroll()
{

	mdi_DiceRoll.EnableWindow(false);
	mdi_DiceRoll.ShowWindow(SW_HIDE);
	m_gameMaster.mbi_DiceRoll = true;
	m_gameMaster.DoState();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


BOOL CDrinkerz2Dlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (mg_redraw == true)
	{
		mg_redraw = false;
		CClientDC dc(this);
		dc.FillSolidRect(CRect(810,0,1110,650), GetSysColor(COLOR_WINDOW));
		dc.FillSolidRect(CRect(0,0,1110,10), GetSysColor(COLOR_WINDOW));
		dc.FillSolidRect(CRect(0,0,10,700), GetSysColor(COLOR_WINDOW));
		dc.FillSolidRect(CRect(0,800,1110,810), GetSysColor(COLOR_WINDOW));
		return true;
	}
	else
	{
		return true;

	}
}
