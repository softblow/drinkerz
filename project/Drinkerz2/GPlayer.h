#pragma once
#include "GSprite.h"
#include "GMap.h"

class CGMap;

class CGPlayer : public CGSprite
{
protected:
	CGMap* m_nowMap;

public:
	// 생성자 및 파괴자
	CGPlayer(UINT nIDRes, CSize frameSize, CPoint pos = CPoint(0,0), int DrinkMax = 1000, int HpMax = 1000, int LuckMax = 1000);
	CGPlayer(UINT nIDRes, CSize frameSize, COLORREF  TransColor, CPoint pos = CPoint(0,0), int DrinkMax = 1000, int HpMax = 1000, int LuckMax = 1000);
	virtual void UpdateImage();
	virtual bool DrawImage(CDC& dc, int w, int h);
	virtual bool DrawImageTrans(CDC& dc, int w, int h);

	void SetMapPos(CGMap* mymap){m_nowMap = mymap;}
	CGMap* GetMapPos() {return m_nowMap;}
	bool GoNext();
	bool GoPrev();

	int GetDrink(){return m_pDrink;}
	int GetHp(){return m_pHp;}
	int GetLuck(){return m_pLuck;}
	void SetDrink(int drink){m_pDrink = max(0,min(m_pDrinkMax,drink));}
	void SetHp(int hp){m_pHp = max(0,min(m_pHpMax,hp));}
	void SetLuck(int luck){m_pLuck = max(0,min(m_pLuckMax,luck));}

public:
	int m_pDrink;	// 주량
	const int m_pDrinkMax;

	int m_pHp;	// 체력
	const int m_pHpMax;

	int m_pLuck;	// 행운
	const int m_pLuckMax;

	int m_pDrinkDisp;
	int m_pHpDisp;
	int m_pLuckDisp;
};
