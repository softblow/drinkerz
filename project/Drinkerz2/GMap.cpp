#pragma once

#include "stdafx.h"
#include "GMap.h"

CGMap::CGMap(CPoint pos) :
	CGImage(m_maptype = RandMapReturn(), pos)
	, m_next(NULL)
	, m_prev(NULL)
{
}

CGMap::CGMap(COLORREF TransColor, CPoint pos) :
	CGImage(m_maptype = RandMapReturn(), TransColor, pos)
	, m_next(NULL)
	, m_prev(NULL)
{
}

CGMap::CGMap(MAPTYPE maptype, CPoint pos) :
	CGImage(maptype, pos)
	, m_next(NULL)
	, m_prev(NULL)
	, m_maptype(maptype)
{
}

CGMap::CGMap(MAPTYPE maptype, COLORREF  TransColor, CPoint pos) :
	CGImage(maptype, TransColor, pos)
	, m_next(NULL)
	, m_prev(NULL)
	, m_maptype(maptype)
{
}

void CGMap::UpdateImage()
{
}

CGMap::MAPTYPE CGMap::RandMapReturn()
{
	switch(rand()%16){
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
			return Normal;
		case 7:
		case 8:
		case 9:
			return Attraction;
		case 10 :
		case 11 :

		case 12 :
			return Event;
		case 13:

		case 14 :
		case 15:
			return Game;
	}
	return Normal;
}

void CGMap::RandMapChange()
{
	m_maptype = RandMapReturn() ;
	m_bitmap.LoadBitmap(m_maptype);
}

