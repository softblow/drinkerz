#include "StdAfx.h"
#include "GTiledBackground.h"


CGTiledBackground::CGTiledBackground(UINT nIDRes, CSize size, CPoint pos) :
	CGImage(nIDRes, pos)
	, m_size(size)
{
}
CGTiledBackground::CGTiledBackground(UINT nIDRes, COLORREF  TransColor, CSize size, CPoint pos)
	:CGImage(nIDRes, TransColor, pos)
	, m_size(size)
{
}



bool CGTiledBackground::DrawImage(CDC& dc, int w, int h)
{
	if (m_bitmap.GetSafeHandle() == NULL)
		return false;

	BITMAP bm;
	m_bitmap.GetBitmap(&bm);
	
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CBitmap* pOldBitmap = (CBitmap *)dcMem.SelectObject(&m_bitmap);

	for (int i = -1; ((i*bm.bmWidth) <= m_size.cx); i++)
		for (int j = -1; ((j*bm.bmHeight) <= m_size.cy); j++)
			dc.BitBlt(m_pos.x + (i*bm.bmWidth), m_pos.y + (j*bm.bmHeight), bm.bmWidth, bm.bmHeight, &dcMem, 0, 0, SRCCOPY);
	
	if (pOldBitmap != NULL)
		dcMem.SelectObject (pOldBitmap);

	return true;
}


bool CGTiledBackground::DrawImageTrans(CDC& dc, int w, int h)
{
	if (m_bitmap.GetSafeHandle() == NULL)
		return false;

	BITMAP bm;
	m_bitmap.GetBitmap(&bm);
	
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CBitmap* pOldBitmap = (CBitmap *)dcMem.SelectObject(&m_bitmap);

	for (int i = 0; ((i*bm.bmWidth) <= m_size.cx); i++)
		for (int j = 0; ((j*bm.bmHeight) <= m_size.cy); j++)
			dc.TransparentBlt(m_pos.x + (i*bm.bmWidth), m_pos.y + (j*bm.bmHeight)
			, min(w, bm.bmWidth), min(h, bm.bmHeight)
			, &dcMem, 0, 0, bm.bmWidth, bm.bmHeight, m_transColor);
	
	if (pOldBitmap != NULL)
		dcMem.SelectObject (pOldBitmap);

	return true;
}