// 게임 다이얼로그입니다. 게임 관리는 별도의 클래스 GameMaster 에서 하고있습니다.
#pragma once

#include "GameMaster.h"

class CGameMaster;

// CDrinkerz2Dlg 대화 상자입니다.

class CDrinkerz2Dlg : public CDialogEx
{
	DECLARE_DYNAMIC(CDrinkerz2Dlg)

public:
	CDrinkerz2Dlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDrinkerz2Dlg();

	friend CGameMaster; // 지친 모습의 흔적...
public:
	CGameMaster m_gameMaster; // 께임
	bool mg_redraw; // 깜빡임 방지를 위해, 화면 지우기를 하지 않기 때문에 필요한 경우 이 변수를 true로 하고 Invalidate() 합니다.
// 대화 상자 데이터입니다.
	enum { IDD = IDD_DRINKERZ2_DIALOG_GAME };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedDiceroll();
	CButton mdi_DiceRoll; // 유일하게 사용되는 버튼
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};
