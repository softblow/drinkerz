// IntroDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Drinkerz2.h"
#include "IntroDlg.h"
#include "Drinkerz2Dlg.h"
#include "afxdialogex.h"


  
// CIntroDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CIntroDlg, CDialogEx)

CIntroDlg::CIntroDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CIntroDlg::IDD, pParent)
{

	EnableAutomation();

}

CIntroDlg::~CIntroDlg()
{
}

void CIntroDlg::OnFinalRelease()
{
	// 자동화 개체에 대한 마지막 참조가 해제되면
	// OnFinalRelease가 호출됩니다. 기본 클래스에서 자동으로 개체를 삭제합니다.
	// 기본 클래스를 호출하기 전에 개체에 필요한 추가 정리 작업을
	// 추가하십시오.

	CDialogEx::OnFinalRelease();
}

void CIntroDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CIntroDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CIntroDlg::OnBnClickedOk)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CIntroDlg, CDialogEx)
END_DISPATCH_MAP()

// 참고: IID_IIntroDlg에 대한 지원을 추가하여
//  VBA에서 형식 안전 바인딩을 지원합니다. 
//  이 IID는 .IDL 파일에 있는 dispinterface의 GUID와 일치해야 합니다.

// {948392E1-4231-49ED-8727-6654506D75C2}
static const IID IID_IIntroDlg =
{ 0x948392E1, 0x4231, 0x49ED, { 0x87, 0x27, 0x66, 0x54, 0x50, 0x6D, 0x75, 0xC2 } };

BEGIN_INTERFACE_MAP(CIntroDlg, CDialogEx)
	INTERFACE_PART(CIntroDlg, IID_IIntroDlg, Dispatch)
END_INTERFACE_MAP()


// CIntroDlg 메시지 처리기입니다.


void CIntroDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	PlaySound(NULL, 0, 0);// Stop Music
	EndDialog(0);

	CDrinkerz2Dlg dlg;
	dlg.DoModal();

	CDialogEx::OnOK();
}


BOOL CIntroDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	PlaySound(MAKEINTRESOURCE(IDRM_INTRO),  GetModuleHandle(NULL), SND_ASYNC | SND_RESOURCE | SND_LOOP);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
