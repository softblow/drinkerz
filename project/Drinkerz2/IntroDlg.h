// 인트로 다이얼로그
#pragma once


// CIntroDlg 대화 상자입니다.

class CIntroDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CIntroDlg)

public:
	CIntroDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CIntroDlg();

	virtual void OnFinalRelease();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_INTRODLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
};
