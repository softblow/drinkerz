// 책에서 복사해와서 게임 스크롤 구조에 알맞게 개조된 CBackDC, 즉 메모리 버퍼 DC.
// 변수 이름을 보세요. 이름 대충 안 지었습니다.

// 메모리에 몽땅 받아와서 그리는 방식이라 성능에는 문제 있습니다. 과제니까요. ㅠㅠ
#pragma once
#include <afxwin.h>

class CBackDC : public CDC
{
public:
	CBackDC (CDC& dc, CWnd* pWnd, CPoint viewport = CPoint(0,0), CSize& viewSize = CSize(800,600)); // 윈도우 크기와 출력 영역 일치
	CBackDC (CDC& dc, CRect& mapSize, CPoint viewport = CPoint(0,0), CSize& viewSize = CSize(800,600)); //  직접 지정한 영역에 출력
	virtual ~CBackDC(void);
	
public:
	LONG GetWidth() {return m_sizeBitmap.cx;}
	LONG GetHeight() {return m_sizeBitmap.cy;}
	
protected:
	void Internal_InitBackBuffer (CDC& dc, CWnd* pWnd);
	void Internal_InitBackBuffer (CDC& dc, CRect& rect);

	void Internal_FreeBackBuffer (CDC& dc);

	
protected:
	CDC& m_dc;
	CWnd* m_pWnd; 
	CRect m_targetRect; // 목적 크기

	CPoint m_viewport; // 현재 스크롤 위치 (viewPort)
	CSize m_viewSize; // 보이는 영역의 크기
	
	CSize m_sizeBitmap;
	CBitmap* m_pOldBitmap;
};