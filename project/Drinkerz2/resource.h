//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Drinkerz2.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DRINKERZ2_DIALOG            102
#define IDD_DRINKERZ2_DIALOG_GAME       102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDD_INTRODLG                    104
#define IDD_RPSGAME                     105
#define IDR_MAINFRAME                   128
#define IDR_MAINMENU                    129
#define IDB_BITMAP2                     133
#define IDB_BITMAP3                     134
#define IDBP_NAHI                       134
#define IDB_BACKGROUND                  136
#define IDBM_ATTRACTION                 137
#define IDB_BITMAP4                     138
#define IDBM_EVENT                      138
#define IDBM_GAME                       139
#define IDBM_REST                       140
#define IDB_BITMAP1                     141
#define IDBP_DAGU                       141
#define IDBM_END                        142
#define IDBD_DICE1                      143
#define IDBD_DICE2                      144
#define IDBD_DICE3                      145
#define IDBD_DICE4                      146
#define IDBD_DICE5                      147
#define IDBD_DICE6                      148
#define IDB_BITMAP11                    149
#define IDBD_DICEROLL                   149
#define IDB_SPLASH                      150
#define IDRM_INTRO                      151
#define IDRM_GAME                       152
#define IDBPP_DAGU                      153
#define IDBPP_NAHI                      154
#define IDBRPS_PAPER                    155
#define IDBRPS_ROCK                     156
#define IDBRPS_SCISSORS                 157
#define IDC_BUTTON1                     1001
#define IDCG_DICEROLL                   1001
#define IDC_RPSTIME                     1005
#define IDCPP_ENEMY                     1009
#define IDCPP_PLAYER                    1010
#define IDCBRTS_ROCK                    1011
#define IDCBRTS_SCISSORS                1012
#define IDCBRTS_PAPEr                   1013
#define IDCBRTS_PAPEG                   1013
#define IDC_E                           1014
#define IDCBRTSEnemy                    1014
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_DRINKERZ_DRINKERZ            32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_32776                        32776
#define ID_32777                        32777
#define ID_DRINKERZ_32778               32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        159
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           106
#endif
#endif
