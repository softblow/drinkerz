// 투명 기능을 지원하는 이미지 출력 객체
// Class Game Image
// 상속 : GSprite
// 상속 : GMap
// 상속 : GTiledBackground
// 상속 : GSprite -> GPlayer
#pragma once
class CGImage
{
public:
	CGImage(UINT nIDRes, CPoint pos = CPoint(0,0));
	CGImage(UINT nIDRes, COLORREF  TransColor, CPoint pos = CPoint(0,0));
	CGImage(HBITMAP hbitmap, CPoint pos = CPoint(0,0));
	CGImage(HBITMAP hbitmap, COLORREF  TransColor, CPoint pos = CPoint(0,0));
	~CGImage(void);


	virtual bool DrawImage(CDC& dc, int w, int h);
	virtual bool DrawImageTrans(CDC& dc, int w, int h);
	virtual void UpdateImage(){};
	
	void SetTransColor(COLORREF TransColor) {m_transColor = TransColor;}
	void SetPivot(POINT pivot) {m_pivot = pivot;}
private:
//http://www.camaswood.com/tech/copy-hbitmap-to-cbitmap/
	// HBitmap -> CBitmap 은 도저히 짤 수 없어서 가져왔습니다. 위 링크 참조
	void CopyBitmap(HBITMAP hBmp, CBitmap *oBmp);

public:
	CPoint m_pos;
protected:
	CPoint m_pivot;
	CBitmap m_bitmap;
	COLORREF m_transColor;

};

