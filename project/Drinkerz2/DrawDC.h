// CBackDC를 개조하면서 결국 쓰지 않았습니다. 쓴 데... 있나? 책에서 그대로 가져왔던 함수.
#pragma once
#include "BackDC.h"

class CDrawDC : public CBackDC
{
public:
	//	pWnd 가 입력되면 Back Buffer 를 생성한다.
	CDrawDC (CDC& dc, CWnd* pWnd = NULL);
	virtual ~CDrawDC (void);

public:
	void DrawBitmap(int x, int y, CBitmap &bitmap);
	void DrawStretchBitmap(int x, int y, int cx, int cy, CBitmap& bitmap);
	void DrawLimitedImage(int x, int y, int w, int h, CBitmap& bitmap);
};