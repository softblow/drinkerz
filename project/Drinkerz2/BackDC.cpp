#pragma once
#include "stdafx.h"
#include "BackDC.h"

CBackDC::CBackDC (CDC& dc, CWnd* pWnd, CPoint viewport, CSize& viewSize)
	: CDC()
	, m_dc(dc)
	, m_pWnd(pWnd)
	, m_sizeBitmap(0, 0)
	, m_pOldBitmap(NULL)
	, m_targetRect(0,0,0,0)
	, m_viewport(viewport)
	, m_viewSize(viewSize)
{
	if (m_pWnd != NULL)
		Internal_InitBackBuffer (m_dc, pWnd);
}

CBackDC::CBackDC (CDC& dc, CRect& mapSize, CPoint viewport, CSize& viewSize)
	: CDC()
	, m_dc(dc)
	, m_sizeBitmap(0, 0)
	, m_pOldBitmap(NULL)
	, m_targetRect(mapSize)
	, m_viewport(viewport)
	, m_viewSize(viewSize)
{
	if (m_pWnd != NULL)
		Internal_InitBackBuffer (m_dc, mapSize);
}

CBackDC::~CBackDC(void)
{
	if (m_pWnd != NULL)
		Internal_FreeBackBuffer (m_dc);
}

void CBackDC::Internal_InitBackBuffer (CDC& dc, CWnd* pWnd)
{
	if (pWnd == NULL)
		return;
	CRect rect;
	pWnd->GetClientRect(rect);
	
	m_sizeBitmap = CSize(rect.Width(), rect.Height());
	
	this->CreateCompatibleDC(&dc);
	
	CBitmap bmBack;
	bmBack.CreateCompatibleBitmap(&dc, m_sizeBitmap.cx, m_sizeBitmap.cy);
	
	m_pOldBitmap = (CBitmap*)this->SelectObject(&bmBack);
}

void CBackDC::Internal_InitBackBuffer (CDC& dc, CRect& rect)
{
	m_sizeBitmap = CSize(rect.Width(), rect.Height());
	
	this->CreateCompatibleDC(&dc);
	
	CBitmap bmBack;
	bmBack.CreateCompatibleBitmap(&dc, m_sizeBitmap.cx, m_sizeBitmap.cy * 2);
	
	m_pOldBitmap = (CBitmap*)this->SelectObject(&bmBack);
}




void CBackDC::Internal_FreeBackBuffer(CDC &dc)
{
	if (dc.GetSafeHdc() == NULL || GetSafeHdc() == NULL)
		return;
	
	dc.BitBlt(m_targetRect.left, m_targetRect.top, m_viewSize.cx, m_viewSize.cy
		, this
		, min(max(m_viewport.x - m_viewSize.cx/2, 0), m_sizeBitmap.cx - m_viewSize.cx)
		, min(max(m_viewport.y - m_viewSize.cx/2, 0), m_sizeBitmap.cy - m_viewSize.cy)
		, SRCCOPY);
	
	if (m_pOldBitmap != NULL)
	{
		CBitmap* pBitmap = this->SelectObject(m_pOldBitmap);
		if (pBitmap != NULL)
			pBitmap->DeleteObject();
	}
	this->DeleteDC();
}


