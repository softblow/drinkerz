// 투명 지원 안 하는 CStatic 이랑 좀 싸웠습니다.
// 투명한 CStatic 출력 기능

#pragma once


// CTransBitStatic

class CTransBitStatic : public CStatic
{
	DECLARE_DYNAMIC(CTransBitStatic)

public:
	CTransBitStatic();
	virtual ~CTransBitStatic();

protected:
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnPaint();
};


