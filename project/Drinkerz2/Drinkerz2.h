// Drinkerz2.cpp 에서 첫 다이얼로그를 띄웁니다. 그 뒤 창 관리는 해당 다이얼로그들의 역할입니다.
// Drinkerz2.h : PROJECT_NAME 응용 프로그램에 대한 주 헤더 파일입니다.
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
#endif

#include "resource.h"		// 주 기호입니다.


// CDrinkerz2App:
// 이 클래스의 구현에 대해서는 Drinkerz2.cpp을 참조하십시오.
//

class CDrinkerz2App : public CWinApp
{
public:
	CDrinkerz2App();

// 재정의입니다.
public:
	virtual BOOL InitInstance();

// 구현입니다.

	DECLARE_MESSAGE_MAP()
};

extern CDrinkerz2App theApp;