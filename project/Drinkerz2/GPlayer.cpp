#include "StdAfx.h"
#include "GPlayer.h"


CGPlayer::CGPlayer(UINT nIDRes, CSize frameSize, CPoint pos,int DrinkMax, int HpMax, int LuckMax):
	CGSprite(nIDRes, frameSize, pos)
	, m_nowMap(NULL)
	, m_pDrink(DrinkMax/2), m_pDrinkMax(DrinkMax) 
	, m_pHp(HpMax/2), m_pHpMax(HpMax) 
	, m_pLuck(LuckMax/2), m_pLuckMax(LuckMax) 
	
{

}


CGPlayer::CGPlayer(UINT nIDRes, CSize frameSize, COLORREF  TransColor, CPoint pos,int DrinkMax, int HpMax, int LuckMax):
	CGSprite(nIDRes, frameSize, TransColor, pos)
	, m_nowMap(NULL)
	, m_pDrink(DrinkMax/2), m_pDrinkMax(DrinkMax) 
	, m_pHp(HpMax/2), m_pHpMax(HpMax) 
	, m_pLuck(LuckMax/2), m_pLuckMax(LuckMax) 
	, m_pDrinkDisp(0), m_pHpDisp(0), m_pLuckDisp(0)
{
}

bool CGPlayer::DrawImage(CDC& dc, int w, int h)
{
	if(! CGSprite::DrawImage(dc, w, h) )

		return false;

	

	
	// font draw
	CFont font;
	font.CreatePointFont(150, CString("����"), &dc);
	dc.SetBkMode(TRANSPARENT);
	HGDIOBJ p_before = dc.SelectObject(font);

	CString my_p_string;
	my_p_string.Format(CString("�ַ� : %d / %d\nHP : %d / %d"/*\n�� : %d / %d\n"*/), m_pDrinkDisp, m_pDrinkMax, m_pHpDisp, m_pHpMax/*, m_pLuckDisp, m_pLuckMax*/);
	
	dc.SetTextColor(RGB(255,255,255));
	for (int i = -1; i <=1; i++)
	{
		for (int j = -1; j <=1; j++)
		{
			dc.DrawText(my_p_string
				, CRect(m_pos.x - 100 + i, m_pos.y + j
					,m_pos.x + 100 + i, m_pos.y + 100 + j), DT_CENTER );
			//TRACE("looop");
		}
	}
	
	dc.SetTextColor(RGB(0,0,0));
	dc.DrawText(my_p_string
			, CRect(m_pos.x - 100, m_pos.y
				,m_pos.x + 100, m_pos.y + 100), DT_CENTER );
	

	dc.SelectObject(p_before);
	return true;
}

bool CGPlayer::DrawImageTrans(CDC& dc, int w, int h)
{
	if(! CGSprite::DrawImageTrans(dc, w, h) )

		return false;

	
	// font draw
	CFont font;
	font.CreatePointFont(150, CString("����"), &dc);
	dc.SetBkMode(TRANSPARENT);
	HGDIOBJ p_before = dc.SelectObject(font);

	CString my_p_string;
	my_p_string.Format(CString("�ַ� : %d / %d\nHP : %d / %d"/*\n�� : %d / %d\n"*/), m_pDrinkDisp, m_pDrinkMax, m_pHpDisp, m_pHpMax/*, m_pLuckDisp, m_pLuckMax*/);
	
	dc.SetTextColor(RGB(255,255,255));
	for (int i = -1; i <=1; i++)
	{
		for (int j = -1; j <=1; j++)
		{
			dc.DrawText(my_p_string
				, CRect(m_pos.x - 100 + i, m_pos.y + j
					,m_pos.x + 100 + i, m_pos.y + 100 + j), DT_CENTER );
			//TRACE("looop");
		}
	}
	
	dc.SetTextColor(RGB(0,0,0));
	dc.DrawText(my_p_string
			, CRect(m_pos.x - 100, m_pos.y
				,m_pos.x + 100, m_pos.y + 100), DT_CENTER );
	

	dc.SelectObject(p_before);

	return true;
}

void CGPlayer::UpdateImage()
{
	CGSprite::UpdateImage();
	
	if (m_nowMap == NULL)
	{
		TRACE("\nCGPlayer SetPos Failure\n");
		return;
	}
	m_pos.x = (LONG)(m_pos.x * 0.8 + (m_nowMap->m_pos.x) * 0.2);
	m_pos.y = (LONG)(m_pos.y * 0.8 + (m_nowMap->m_pos.y) * 0.2);
	
	for (int i = 0; i < 1 + abs(m_pDrinkDisp - m_pDrink) / 5 ; i++)
	{
		if (m_pDrinkDisp > m_pDrink)
			m_pDrinkDisp--;
		else if (m_pDrinkDisp < m_pDrink)
			m_pDrinkDisp++;
	}
	for (int i = 0; i < 1 + abs(m_pHp - m_pHpDisp) / 5 ; i++)
	{
		if (m_pHpDisp > m_pHp)
			m_pHpDisp--;
		else if (m_pHpDisp < m_pHp)
			m_pHpDisp++;
	}
	for (int i = 0; i < 1 + abs(m_pLuck - m_pLuckDisp) / 5 ; i++)
	{
		if (m_pLuckDisp > m_pLuck)
			m_pLuckDisp--;
		else if (m_pLuckDisp < m_pLuck)
			m_pLuckDisp++;
	}
}

bool CGPlayer::GoNext()
{
	if (m_nowMap->m_next == NULL)
		return false;
	m_nowMap = m_nowMap->m_next;
		return true;
}

bool CGPlayer::GoPrev()
{
	if (m_nowMap->m_prev == NULL)
		return false;
	m_nowMap = m_nowMap->m_prev;
		return true;
}